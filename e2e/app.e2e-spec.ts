import { JangularPage } from './app.po';

describe('jangular App', function() {
  let page: JangularPage;

  beforeEach(() => {
    page = new JangularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
