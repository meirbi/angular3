import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';

@Component({
  selector: 'hadardasim-posts',
  templateUrl: './posts.component.html',
  styles: [`
    .posts li { cursor: default; }
    .posts li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover 
    .list-group-item.active:focus {  
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class PostsComponent implements OnInit {

  isLoading:Boolean = true;
  posts;

  currentPost;

  select(post){
    this.currentPost = post;
  }
  constructor(private _postsService:PostsService) { }

 // addPost(post){  //ex 6        delete it in ex 7
 // this.posts.push(post)
 // }
  addPost(post){                         /// ex 7     create this + the one in service for add a new post to firebase
    this._postsService.addPost(post);
  }
  updatePost(post){                              // ex 7     beafore that there were changes in posts.service.ts
    this._postsService.updatePost(post);          // until now 2 changes to create updatepost
  }
  /*          delete this in ex 7
  deletePost(post){
    this.posts.splice(
      this.posts.indexOf(post),1
    )
  }
  */
    deletePost(post){                             /// create this in ex 7  and after that posts.service.ts
      this._postsService.deletePost(post);
    }
  /*   delete that in ex 7 class when updating a post
  cancel(post){  // for ex 5    
    //this._usersService.updateUser(user); why to use 'usersService' ??? same code like users.component.ts
    let post1 = {title:post.title,body:post.body,author:post.author};
    console.log(this.posts);
  }
  */

  ngOnInit() {
    this._postsService.getPosts().subscribe(postsData =>
    {this.posts = postsData;
    this.isLoading = false
    console.log(this.posts)}); // ex 7
  }

}
