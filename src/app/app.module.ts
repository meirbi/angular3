import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule,Routes } from '@angular/router';  // ex 6 class
import { AngularFireModule} from 'angularfire2';   /// ex 7 class

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { PostsService } from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PostFormComponent } from './post-form/post-form.component';

const appRoutes:Routes = [    // ex 6 class
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},
  {path:'',component:UsersComponent},
  {path:'**',component:PageNotFoundComponent},
]


  // Initialize Firebase  ex 7 class
  export const firebaseConfig = {
    apiKey: "AIzaSyBtXGomQToZ7kjRdd8vDIzkj3m0dfA8Hjk",
    authDomain: "liorangularhome.firebaseapp.com",
    databaseURL: "https://liorangularhome.firebaseio.com",
    storageBucket: "liorangularhome.appspot.com",
    messagingSenderId: "151838224942"
  };


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    UserComponent,
    PostComponent,
    SpinnerComponent,
    PageNotFoundComponent,
    UserFormComponent,
    PostFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,  // ex 6 class
    RouterModule.forRoot(appRoutes),  // ex 6 class
    AngularFireModule.initializeApp(firebaseConfig)   /// ex 7 class
  ],
  providers: [UsersService, PostsService],
  bootstrap: [AppComponent]
  
  
})
export class AppModule { }
