import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from '../post/post';  // ex 6
import {NgForm} from '@angular/forms'; // ex 6

@Component({
  selector: 'hadardasim-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  @Output() postAddedEvent = new EventEmitter<Post>();  // ex 6
  post:Post = {title:'', body:'',author:''};  // ex 6

   onSubmitPost(form:NgForm){  // ex 6
   // console.log('it worked!');   ex 6, to check if the button 'submit' working fine
   console.log(form);
   this.postAddedEvent.emit(this.post);
   this.post = {
     title:'',
     body:'',
     author:'',
   }
  }

  constructor() { }

  ngOnInit() {
  }

}
